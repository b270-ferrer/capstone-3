import { Link } from "react-router-dom";
import React, { useContext } from 'react';
import AuthContext from './AuthContext';

const Navbar = () => {
  const { isLoggedIn, user, logout } = useContext(AuthContext);

  return (
    <nav className="navbar">
      <div className="top-right-links" style={{ display: "flex", justifyContent: "space-between", width: "100%" }}>
        {isLoggedIn ? (
          <>
            <h4 style={{ marginRight: "10px" }}>Hello, {user && user.name}</h4>
            <div>
              {!user.isAdmin && (
                <Link to="/cart">
                  <img
                    src={process.env.PUBLIC_URL + "/images/cart.png"}
                    alt="Cart"
                    className="cart-icon"
                    style={{ width: "40px", marginRight: "5px" }}
                  />
                </Link>
              )}
              {user.isAdmin && (
                <Link to="/products/all">
                  Dashboard
                </Link>
              )}

              <button onClick={logout}>Logout</button>
            </div>
          </>
        ) : (
          <>
            <Link to="/register">Register</Link>
            <Link to="/login">Please Login</Link>
          </>
        )}
      </div>
      <div className="navbar-logo-container">
        <span className="logo-wrapper" style={{ display: "flex", justifyContent: "center", alignItems: "center", width: "100%" }}>
          <Link to="/" className="navbar-logo-link">
            <img
              src={process.env.PUBLIC_URL + "/images/USEFUL GADGET SHOP LOGO.jpg"}
              alt="Shop Logo"
              className="navbar-logo"
              style={{ width: "100%", marginLeft: "10px" }}
            />
          </Link>
        </span>
      </div>
    </nav>
  );
}

export default Navbar;
