import React, { useState, useEffect, useContext } from 'react';
import AuthContext from '../AuthContext';
import Table from 'react-bootstrap/Table';
import 'bootstrap/dist/css/bootstrap.css';

const AllProducts = () => {
  const { user } = useContext(AuthContext);
  const [allProduct, setAllProduct] = useState([]);
  const [editProductId, setEditProductId] = useState(null);
  const [showAddForm, setShowAddForm] = useState(false);
  const [newProduct, setNewProduct] = useState({
    name: '',
    price: '',
    description: '',
  });

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        if (user && user.token) {
          const response = await fetch('http://localhost:4000/products/all', {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${user.token}`,
            },
          });
          const data = await response.json();
          console.log(data);
          setAllProduct(data);
        }
      } catch (error) {
        console.log('Error fetching products:', error);
      }
    };

    fetchProducts();
  }, [user]);

  if (!user) {
    return null;
  }

  const handleIsActive = async (productID, productIsActive) => {
    try {
      const response = await fetch(`http://localhost:4000/products/${productID}/archive`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify({
          isActive: !productIsActive,
        }),
      });

      if (response.ok) {
        const updatedProducts = allProduct.map((product) => {
          if (product._id === productID) {
            return { ...product, isActive: !productIsActive };
          }
          return product;
        });
        setAllProduct(updatedProducts);
      } else {
        console.log('Error updating product isActive status');
      }
    } catch (error) {
      console.log('Error updating product isActive status:', error);
    }
  };

  const handleEdit = (productID) => {
    setEditProductId(productID);
  };

  const handleSave = async (productID) => {
    const productToEdit = allProduct.find((product) => product._id === productID);
    try {
      const response = await fetch(`http://localhost:4000/products/${productID}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify(productToEdit),
      });

      if (response.ok) {
        setEditProductId(null);
        // Handle success or update the state if needed
      } else {
        console.log('Error updating product');
      }
    } catch (error) {
      console.log('Error updating product:', error);
    }
  };

  const handleInputChange = (productID, field, value) => {
    const updatedProducts = allProduct.map((product) => {
      if (product._id === productID) {
        return { ...product, [field]: value };
      }
      return product;
    });
    setAllProduct(updatedProducts);
  };

  const handleAddProduct = () => {
    setShowAddForm(true);
  };

  const handleCancelAddProduct = () => {
    setShowAddForm(false);
    setNewProduct({ name: '', price: '', description: '' });
  };

  const handleSaveNewProduct = async () => {
    try {
      const response = await fetch('http://localhost:4000/products/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
        body: JSON.stringify(newProduct),
      });

      if (response.ok) {
        const data = await response.json();
        const newProductWithId = { ...data.product };

        setAllProduct((prevProducts) => [...prevProducts, newProductWithId]);
        setShowAddForm(false);
        setNewProduct({ name: '', price: '', description: '' });
      } else {
        console.log('Error adding new product');
      }
    } catch (error) {
      console.log('Error adding new product:', error);
    }
  };

  const handleFileUpload = async (event, filename) => {
    const file = event.target.files[0];
    const formData = new FormData();
    formData.append('file', file);
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/${newProduct.name}/${filename}`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
        body: formData,
      });

      if (response.ok) {
        console.log(`${filename} uploaded successfully`);
      } else {
        console.log(`Error uploading ${filename}`);
      }
    } catch (error) {
      console.log(`Error uploading ${filename}:`, error);
    }
  };

  return (
    <div className="allproducts">
      <p>All Products:</p>
      <button onClick={handleAddProduct}>Add Product</button>
      {showAddForm && (
        <React.Fragment>
          <Table striped bordered hover>
            <tbody>
              <tr>
                <td>
                  <button onClick={handleSaveNewProduct}>Save</button>
                  <button onClick={handleCancelAddProduct}>Cancel</button>
                </td>
                <td>
                  <input
                    type="text"
                    value={newProduct.name}
                    onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
                  />
                </td>
                <td>
                  <input
                    type="text"
                    value={newProduct.price}
                    onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="3">
                  <textarea
                    value={newProduct.description}
                    onChange={(e) =>
                      setNewProduct({ ...newProduct, description: e.target.value })
                    }
                    style={{ width: '100%' }}
                  />
                </td>
              </tr>
              <tr>
                <td colSpan="3">
                  <input type="file" accept="image/jpeg" onChange={(e) => handleFileUpload(e, 'image.jpg')} />
                  <input type="file" accept="image/gif" onChange={(e) => handleFileUpload(e, 'animated.gif')} />
                </td>
              </tr>
            </tbody>
          </Table>
          <hr />
        </React.Fragment>
      )}
      <Table striped bordered hover>
        <tbody>
          {allProduct.map((product) => (
            <React.Fragment key={product._id}>
              {product.name && product.price && product.description && (
                <React.Fragment>
                  <tr>
                    <td>
                      {editProductId === product._id ? (
                        <button onClick={() => handleSave(product._id)}>Save</button>
                      ) : (
                        <button onClick={() => handleIsActive(product._id, product.isActive)}>
                          {product.isActive ? 'Active' : 'Archived'}
                        </button>
                      )}
                      <button onClick={() => handleEdit(product._id)}>
                        {editProductId === product._id ? 'Cancel' : 'Edit'}
                      </button>
                    </td>
                    <td>
                      {editProductId === product._id ? (
                        <React.Fragment>
                          <input
                            type="text"
                            value={product.name}
                            onChange={(e) =>
                              handleInputChange(product._id, 'name', e.target.value)
                            }
                          />
                        </React.Fragment>
                      ) : (
                        product.name
                      )}
                    </td>
                    <td>
                      {editProductId === product._id ? (
                        <input
                          type="text"
                          value={product.price}
                          onChange={(e) =>
                            handleInputChange(product._id, 'price', e.target.value)
                          }
                        />
                      ) : (
                        `Price: ${product.price}`
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="3">
                      {editProductId === product._id ? (
                        <textarea
                          value={product.description}
                          onChange={(e) =>
                            handleInputChange(product._id, 'description', e.target.value)
                          }
                          style={{ width: '100%' }}
                        />
                      ) : (
                        product.description
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="3">{product._id}</td>
                  </tr>
                </React.Fragment>
              )}
            </React.Fragment>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default AllProducts;
