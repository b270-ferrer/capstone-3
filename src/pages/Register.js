//Register.js
import { useState } from "react";
import { useNavigate} from "react-router-dom";
import React, { useContext } from 'react';
import AuthContext from '../AuthContext';

// stc + tab  (stateless functional component)
const Register = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [isPending, setIsPending] = useState(false);
    const navigate = useNavigate();

    const {auth, user} = useContext(AuthContext);
    user.isAdmin=false;



    const handleRegister = e =>{
        e.preventDefault();
        const newUser = { name, email, password, mobileNo, isAdmin:false};


        setIsPending(true);
        
        //fetch( 'https://e-commerce-api-6iky.onrender.com/users/register',{

        fetch( `${process.env.REACT_APP_API_URL}/users/register`,{
            method: 'POST',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify (newUser)
        }).then(()=>{
            console.log("new user added")
        //    setTimeout(() => {
                setIsPending(false);                
         //   }, 1000);
         //   setTimeout(() => {
                auth.login();
         //   }, 1000);            

         //   setTimeout(() => {                
                navigate("/");
         //   }, 1000);
        })
    }

    return (  
        <div className="register">
            <h2> Add a New Account </h2>
            <form onSubmit={ handleRegister }>
                <label>Name:</label>
                <input 
                    type="text"
                    required    
                    value={ name }
                    onChange={ e=>setName(e.target.value) }
                />
                <label>Email:</label>
                <input 
                    type="text"
                    required    
                    value={ email }
                    onChange={ e=>setEmail(e.target.value) }
                />
                <label>Password:</label>
                <input 
                    type="text"
                    required    
                    value={ password }
                    onChange={ e=>setPassword(e.target.value) }
                />
                <label>Mobile Number:</label>
                <input 
                    type="text"
                    required    
                    value={ mobileNo }
                    onChange={ e=>setMobileNo(e.target.value) }
                />                

     
                { !isPending && <button>Register New Account</button>}
                { isPending && <button disabled>Adding New Account...</button>}

            </form>            
        </div>

    );
}
 

export default Register;