import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import AuthContext from '../AuthContext';
import { useNavigate } from 'react-router-dom';

const Product = () => {
  const { productId } = useParams();
  const [product, setProduct] = useState(null);
  const [isHovered, setIsHovered] = useState(false);
  const [isClicked, setIsClicked] = useState(false);
  const { isLoggedIn, user, addProduct } = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => setProduct(data))
      .catch((error) => console.log(error));
  }, [productId]);

  if (!product) {
    return <div>Loading...</div>;
  }

  const handleAddToCart = (productId) => {
    addProduct(productId);
    setIsClicked(true);
  };

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
    setIsClicked(false); // Reset the click state when the mouse leaves
  };

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Button onClick={() => navigate(`/`)}>Back to Products</Button>
      </div>
      <h2>{product.name}</h2>
      <img
        src={`${process.env.PUBLIC_URL}/images/${product.name}/animated.gif`}
        alt=""
        style={{ width: '400px' }}
      />
      <img
        src={`${process.env.PUBLIC_URL}/images/${product.name}/image.jpg`}
        alt=""
        style={{ width: '400px' }}
      />
      
      <p>Description: {product.description}</p>
      <p>Price: {product.price}</p>
      {/*<p>Quantity: {product.quantity}</p>*/}
      {isLoggedIn && !user.isAdmin && (
        <img
          src={`${process.env.PUBLIC_URL}/images/add to cart.webp`}
          alt="Add to Cart"
          onClick={() => handleAddToCart(product._id)}
          style={{
            width: '100px',
            cursor: 'pointer',
            filter: isHovered ? 'scale(1.2)' : 'scale(1)',
            transform: isClicked ? 'scale(1.4)' : 'scale(1)',
          }}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        />
      )}
    </div>
  );
};

export default Product;
