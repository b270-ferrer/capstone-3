// Login.js
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import React, { useContext } from 'react';
import AuthContext from '../AuthContext';
import Swal from 'sweetalert2';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isPending, setIsPending] = useState(false);
  const navigate = useNavigate();
  const auth = useContext(AuthContext);

  const handleLogin = (e) => {
    e.preventDefault();
    setIsPending(true);

    fetch('http://localhost:4000/users/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.accessToken === undefined) {
          Swal.fire({
            title: 'Authentication failed!',
            icon: 'error',
            text: 'Check your login credentials and try again!',
          });
          navigate('/');
        } else {
          let userAccessToken = data.accessToken;
          fetch(`${process.env.REACT_APP_API_URL}/users/`, {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${userAccessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              auth.login({
                id: data._id,
                isAdmin: data.isAdmin,
                token: userAccessToken,
                name: data.name
              });
              setIsPending(false);
              if (data.isAdmin){
                navigate('/products/all');
              }else{
                navigate('/');
              }
              
            });
        }
      });
  };

  return (
        <div className="login">
            <h2> Login </h2>
            <form onSubmit={ handleLogin }>
                <label>Email:</label>
                <input 
                    type="text"
                    required    
                    value={ email }
                    onChange={ e=>setEmail(e.target.value) }
                />
                <label>Password:</label>
                <input 
                    type="text"
                    required    
                    value={ password }
                    onChange={ e=>setPassword(e.target.value) }
                />                  
                { !isPending && <button>Login</button>}
                { isPending && <button disabled>Logging In ...</button>}
            </form>            
        </div>

    );
}
 

export default Login;