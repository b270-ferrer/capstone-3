import React, { useContext, useEffect, useState } from 'react';
import AuthContext from '../AuthContext';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

const CheckoutCart = () => {
  const { user, cart, clearCart, addProduct, subtractProduct } = useContext(AuthContext);
  //const { user, cart, addProduct, subtractProduct } = useContext(AuthContext);
  const [productData, setProductData] = useState([]);
  const navigate = useNavigate();

  const handleAddToCart = async (productId) => {
    await addProduct(productId);
    console.log ("productId = "+productId)
    console.log (JSON.stringify(cart))
  };

  const handleSubtractFromCart = async (productId) => {
    await subtractProduct(productId, 1);
  };

 const handleCheckout = async () => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify(cart),
    });

    if (response.ok) {
      Swal.fire({
        title: 'Successfully Purchased',
        icon: 'success',
        text: 'You have successfully purchased.',
      });
      // Return to '/'
      navigate('/');

        // Waits 1 second and then reset cart
        setTimeout(() => {
          clearCart();
        }, 1000);

    } else {
      Swal.fire({
        title: 'Something went wrong',
        icon: 'error',
        text: 'Please try again.',
      });
    }
  } catch (error) {
    // Error occurred during checkout
    console.log('Error:', error);
  }
};

  useEffect(() => {
    const fetchProductData = async () => {
      const productIds = cart.products.map((product) => product.productId);
      const productData = await Promise.all(
        productIds.map((id) =>
          fetch(`http://localhost:4000/products/${id}`, { method: 'GET' })
            .then((res) => res.json())
            .catch((error) => {
              console.log('Error:', error);
              return null;
            })
        )
      );
      setProductData(productData.filter((product) => product !== null));
    };

    if (cart.products.length > 0) {
      fetchProductData();
    }
  }, [cart.products]);

  const getTotal = () => {
    let total = 0;
    productData.forEach((product, index) => {
      const cartProduct = cart.products[index];
      total += product.price * cartProduct.quantity;
    });
    return total;
  };

  return (
    <div className="checkout-cart">
      <div className="checkout-cart-header">
        <h2>Checkout Cart</h2>
        {productData.length > 0 && (
          <button onClick={handleCheckout}>Checkout</button>
        )}
      </div>
      {cart.products.length === 0 ? (
        <p>Your cart is empty.</p>
      ) : (
        <table>
          <thead>
            <tr>
              <th>Product Image</th>
              <th>Product Name</th>
              <th>Price</th>
              <th>Quantity</th>
              {/*<th>User ID</th>*/}
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {productData.map((product, index) => {
              const cartProduct = cart.products[index];
              return (
                <tr key={product.productId}>
                  <td>
                    <img
                      src={`${process.env.PUBLIC_URL}/images/${product.name}/image.jpg`}
                      alt="Product"
                      style={{ width: '100px' }}
                    />
                  </td>

                  <td>{product.name}</td>
                  <td>{product.price}</td>
                  <td>{cartProduct.quantity}</td>
                  {/*<td>{user.id}</td>*/}
                  {/*<td>{product._id}</td>*/}
                  <td>
                    <button onClick={() => handleAddToCart(product._id)}>+</button>
                  </td>
                  <td>
                    <button onClick={() => handleSubtractFromCart(product._id)}>-</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      {productData.length > 0 && (
        <p>Total: ${getTotal().toFixed(2)}</p>
      )}
    </div>
  );
};

export default CheckoutCart;
